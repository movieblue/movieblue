import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule,
   CarouselModule,
   CardsModule,
   ButtonsModule,
   IconsModule } from 'angular-bootstrap-md';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MDBBootstrapModule,
    CarouselModule,
    ButtonsModule,
    IconsModule,
    MDBBootstrapModule.forRoot()
  ],
  exports: [
    CommonModule,
    MDBBootstrapModule,
    CarouselModule,
    ButtonsModule,
    IconsModule,
    MDBBootstrapModule
  ]
})
export class BootstrapmdModule { }
