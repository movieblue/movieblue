import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { BootstrapmdModule } from './bootstrapmd/bootstrapmd.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MaterialModule,
    BootstrapmdModule
  ]
})
export class SharedModule { }
