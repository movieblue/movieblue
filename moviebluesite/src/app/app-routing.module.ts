import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './core/components/home/home.component';
import { PagenotfoundComponent } from './core/components/pagenotfound/pagenotfound.component';
import { CommonModule } from '@angular/common';
import { MoviesListComponent } from './core/components/movies-list/movies-list.component';
import { PeopleListComponent } from './core/components/people-list/people-list.component';
import { TvListComponent } from './core/components/tv-list/tv-list.component';
import { OngletsComponent } from './core/components/onglets/onglets.component';

export const routes: Routes = [
  {
    path: 'accueil',
    component: HomeComponent
  },
  {
    path: '',
    redirectTo: '/accueil',
    pathMatch: 'full'
  },
  {
    path: '**/*',
    component: PagenotfoundComponent
  },
  {
    path: 'movies',
    loadChildren: './core/core.module#CoreModule'
  },
  {
    path: 'tv',
    loadChildren: './core/core.module#CoreModule'
  },
  {
    path: 'people',
    loadChildren: './core/core.module#CoreModule'
  },
  {
    path: 'movies-list/:recherche',
    component: MoviesListComponent
  },
  {
    path: 'people-list/:recherche',
    component: PeopleListComponent
  },
  {
    path: 'tv-list/:recherche',
    component: TvListComponent
  },
  {
    path: 'onglets/:recherche/:code',
    component: OngletsComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})

































export class AppRoutingModule { }
