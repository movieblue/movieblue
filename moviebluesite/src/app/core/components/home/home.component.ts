import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  films = [];
  tv = [];

  constructor(private httpRequest: HttpRequestService) { }

  ngOnInit() {
    this.getLatestMovies();
    this.getLatestShows();
  }

  getLatestMovies() {
    this.httpRequest.getLatestMovies().subscribe((res: any) => {
      console.log(res);
      this.films = res.results;
    }, err => {
      console.log(err);
    });
  }

  getLatestShows() {
    this.httpRequest.getLatestShows().subscribe((res: any) => {
      console.log(res);
      this.tv = res.results;
    }, err => {
      console.log(err);
    });
  }

}
