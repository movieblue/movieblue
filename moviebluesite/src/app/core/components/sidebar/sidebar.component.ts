import { Router, ActivatedRoute, Params } from '@angular/router';
import { HostListener, Component, ViewChild, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { MatSidenav } from '@angular/material';
import { FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy{
  recherche: string;
  resultats = [];
  flag: boolean;
  path: string;
  opened = true;
  rout: Subscription;
  searchForm: FormGroup;
  languages: any = [];
  language: any = {};

  @ViewChild('closeDropdown', { static: true }) el: ElementRef; // Getting access to the dom element through ViewChild
  @ViewChild('sidenav', { static: true }) sidenav: MatSidenav;
  constructor(private httpRequest: HttpRequestService, private router: Router,
              private route: ActivatedRoute, private sanitizer: DomSanitizer) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
    }

  ngOnInit() {
    if (window.innerWidth < 768) {
      this.sidenav.fixedTopGap = 55;
      this.opened = true;
    } else {
      this.sidenav.fixedTopGap = 55;
      this.opened = false;
    }
    this.rout = this.route.params.subscribe(
        (params: Params) => {
           this.recherche = params.recherche;
        });
    this.search();
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if (event.target.innerWidth < 768) {
      this.sidenav.fixedTopGap = 55;
      this.opened = false;
    } else {
      this.sidenav.fixedTopGap = 55;
      this.opened = true;
    }
  }

  isBiggerScreen() {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width < 768) {
      return true;
    } else {
      return false;
    }
  }

  search() {
    console.log('flag :' + this.flag);
    console.log(encodeURIComponent(this.recherche));    // Encode la recherche pour autoriser les caractères spéciaux

    this.httpRequest.search(encodeURIComponent(this.recherche)).subscribe((res: any) => {
      this.resultats = res.results.slice(0, 5);   // Limite à 5 résultats
    }, err => {
      console.log(err);
    });
  }

  // TODO : Trouver alternative au (blur) ?

  reset(id: number) {
    console.log('recherche :' + this.recherche);
    console.log('id :' + id);
    this.recherche = '';
    this.path = '/people-details/' + id;
    console.log(this.path);
    // this.router.navigate([this.path]); // .then(() =>  { window.location.reload(); });
    // [routerLink]="['/people-details', resultat.id]"
  }

  stop() {
    this.flag = false;
    console.log('stop');
  }

  begin() {
    this.flag = true;
    console.log('begin');
  }

  hideDropdown() {
  this.el.nativeElement.style.display = 'none';
  }


  ngOnDestroy() {
    this.rout.unsubscribe();
  }
}
