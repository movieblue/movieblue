import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-onglets',
  templateUrl: './onglets.component.html',
  styleUrls: ['./onglets.component.scss']
})
export class OngletsComponent implements OnInit, OnDestroy {

  recherche: string;
  code: number;
  rout: Subscription;

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.rout = this.route.params.subscribe(
      (params: Params) => {
         this.recherche = params.recherche;
         this.code = params.code;
      }
   );
  }

  ngOnDestroy() {
    this.rout.unsubscribe();
  }


}
