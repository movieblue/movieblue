import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Params, ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-tv-details',
  templateUrl: './tv-details.component.html',
  styleUrls: ['./tv-details.component.scss']
})
export class TvDetailsComponent implements OnInit, OnDestroy {
  id: string;
  details: any;
  cast = [];
  crew = [];
  similar = [];
  videos = [];
  rout: Subscription;

  constructor(private httpRequest: HttpRequestService, private route: ActivatedRoute,
              private sanitizer: DomSanitizer) {}

     ngOnInit() {
      this.rout = this.route.params.subscribe(
        (params: Params) => {
           this.id = params.id;
        }
     );
      this.getTVDetails();
      this.getTVVideos();
      this.getRecommendations();
      this.getCast();
    }

  getTVDetails() {
    this.httpRequest.getTVDetails(this.id).subscribe((res: any) => {
      console.log(res);
      this.details = res;
    }, err => {
      console.log(err);
    });
  }

  getTVVideos() {
    this.httpRequest.getTVVideos(this.id).subscribe((res: any) => {
      console.log(res);
      this.videos = res.results;
    }, err => {
      console.log(err);
    });
  }

  getCast() {
    this.httpRequest.getCast(this.id, 'tv').subscribe((res: any) => {
      console.log(res);
      this.cast = res.cast;
      this.crew = res.crew;
    }, err => {
      console.log(err);
    });
  }

  getRecommendations() {
    this.httpRequest.getRecommendations(this.id, 'tv').subscribe((res: any) => {
      console.log(res);
      this.similar = res.results;
    }, err => {
      console.log(err);
    });
  }

  videoURL(key: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + key);
  }

  ngOnDestroy() {
      this.rout.unsubscribe();
  }

}
