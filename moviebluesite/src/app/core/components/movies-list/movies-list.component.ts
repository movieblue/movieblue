import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss'],
   animations: [
    trigger('movieAppeared', [
      state('', style({opacity: 1})),
      transition('void => ready', [
        style({opacity: 0, transform: 'translate(-30px, -10px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ])
  ]
})
export class MoviesListComponent implements OnInit, OnDestroy {
  recherche: string;
  films = [];
  movieState = 'ready';
  rout: Subscription;
  page = 1;
  maxPage: number;
  resultats: number;

  constructor(private httpRequest: HttpRequestService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.rout = this.route.params.subscribe(
      (params: Params) => {
         this.recherche = params.recherche;
      }
   );

    console.log('recherche : ' + this.recherche);
    this.search();
}

  search() {
    console.log(this.recherche);
    console.log(encodeURIComponent(this.recherche));    // Encode la recherche pour autoriser les caractères spéciaux

    this.httpRequest.searchMovies(encodeURIComponent(this.recherche), this.page).subscribe((res: any) => {
      console.log(res);
      this.films = res.results;
      this.maxPage = res.total_pages;
      this.resultats = res.total_results;
    }, err => {
        console.log(err);
    });

    // TODO : Enchainer les recherches ?
}

  movePage(move: number) {

    if (move === 0) {
      this.page += 1;
    } else
    if (move === -1) {
      this.page -= 1;
    } else {
      this.page = move;
    }

    console.log('page : ' + this.page);

    this.search();
}

  ngOnDestroy() {
  this.rout.unsubscribe();
}

}
