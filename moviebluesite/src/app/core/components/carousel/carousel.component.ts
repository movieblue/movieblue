import { Component, OnInit } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.scss']
})
export class CarouselComponent implements OnInit {

  filmsCine = [];
  films = [];
  tv = [];
  trailers = [];
  index = 0;

  constructor(private httpRequest: HttpRequestService, private sanitizer: DomSanitizer) {
    this.getMoviesInTheater();    // Appel avant ngOnInit pour assurer qu'il soit appelé en premier
   }

  ngOnInit() {
  this.getLatestMovies();
  this.getLatestShows();
  this.getTrailers();
  }

  getMoviesInTheater() {
    this.httpRequest.getMoviesInTheater().subscribe((res: any) => {
      console.log(res);
      this.filmsCine = res.results;
    }, err => {
      console.log(err);
    });
  }

  getTrailers() {
    this.filmsCine.forEach(element => {
      this.httpRequest.getMovieVideos(this.filmsCine[this.index].id).subscribe((res: any) => {
        console.log(res);
        this.trailers.push(res.results[0].key);
      }, err => {
        console.log(err);
      });
    });
  }

  videoURL(key: string) {
    console.log('trailers :' + this.trailers);
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + key);
  }

  getLatestMovies() {
    this.httpRequest.getLatestMovies().subscribe((res: any) => {
      console.log(res);
      this.films = res.results.slice(0, 10);   // Limite à 10 résultats
    }, err => {
      console.log(err);
    });
  }

  getLatestShows() {
    this.httpRequest.getLatestShows().subscribe((res: any) => {
      console.log(res);
      this.tv = res.results.slice(0, 10);   // Limite à 10 résultats
    }, err => {
      console.log(err);
    });
  }
}
