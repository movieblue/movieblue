import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit, OnDestroy {
  id: string;
  details: any;
  similar = [];
  rout: Subscription;
  videos = [];
  cast = [];
  crew = [];
  private parametersObservable: any;

  constructor(private httpRequest: HttpRequestService, private router: Router,
              private route: ActivatedRoute, private sanitizer: DomSanitizer) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
   }

  ngOnInit() {
      this.rout = this.route.params.subscribe(
        (params: Params) => {
           this.id = params.id;
        });

      this.getMovieDetails();
      this.getMovieVideos();
      this.getRecommendations();
      this.getCast();
    }



  getMovieDetails() {
    this.httpRequest.getMovieDetails(this.id).subscribe((res: any) => {
      console.log(res);
      this.details = res;
    }, err => {
      console.log(err);
    });
  }

  getRecommendations() {
    this.httpRequest.getRecommendations(this.id, 'movie').subscribe((res: any) => {
      console.log(res);
      this.similar = res.results;
    }, err => {
      console.log(err);
    });
  }

  getMovieVideos() {
    this.httpRequest.getMovieVideos(this.id).subscribe((res: any) => {
      console.log(res);
      this.videos = res.results;
    }, err => {
      console.log(err);
    });
  }

  getCast() {
    this.httpRequest.getCast(this.id, 'movie').subscribe((res: any) => {
      console.log(res);
      this.cast = res.cast;
      this.crew = res.crew;
    }, err => {
      console.log(err);
    });
  }

  videoURL(key: string) {
    return this.sanitizer.bypassSecurityTrustResourceUrl('https://www.youtube.com/embed/' + key);
  }

  ngOnDestroy() {
    this.rout.unsubscribe();
  }

}
