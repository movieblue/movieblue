import { Component, OnInit, Input } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { style, state, trigger, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-all-tv',
  templateUrl: './all-tv.component.html',
  styleUrls: ['./all-tv.component.scss']
})
export class AllTvComponent implements OnInit {
  tv = [];
  page = 1;
  maxPage: number;
  dates = ['1800-1900', '1901-1920', '1921-1940', '1941-1960', '1961-1980', '1981-2000', '2000-2010', '2011-2020', '2020-2030'];
  selectedDates = [];
  genres = [];
  selectedGenres = [];
  tri = '1';

  constructor(private httpRequest: HttpRequestService) { }

  ngOnInit() {
    this.httpRequest.getGenreList('tv').subscribe((res: any) => {
      console.log(res);
      this.genres = res.genres;
    }, err => {
      console.log(err);
    });

    this.getMostPopularTV();
  }

  getMostPopularTV() {
    this.httpRequest.getMostPopularTV(this.page, this.tri, this.selectedGenres, this.selectedDates).subscribe((res: any) => {
      console.log(res);
      this.tv = res.results;
      this.maxPage = res.total_pages;
    }, err => {
      console.log(err);
    });
  }

  movePage(move: number) {

    if (move === 0) {
      this.page += 1;
    } else
    if (move === -1) {
      this.page -= 1;
    } else {
      this.page = move;
    }

    console.log('page : ' + this.page);

    this.getMostPopularTV();
}

  selectG(id: string) {
    this.selectedGenres.push(id);
    console.log(this.selectedGenres);

    this.getMostPopularTV();
  }

  deleteG(id: string) {
    let i: number;
    for (i = 0; i < this.selectedGenres.length; i++) {    // supprime le genre correspondant
      if (this.selectedGenres[i] === id) {
        this.selectedGenres.splice(i, 1);
      }
    }

    this.getMostPopularTV();
  }

  selectD(date: string) {
    this.selectedDates[0] = date.split('-')[0];
    this.selectedDates[1] = date.split('-')[1];

    this.getMostPopularTV();
  }

  sort(num: string) {
    this.tri = num;
    this.getMostPopularTV();
  }

}
