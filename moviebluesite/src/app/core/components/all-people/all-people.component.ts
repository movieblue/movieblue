import { Component, OnInit } from '@angular/core';
import {HttpRequestService} from 'src/app/services/http-request.service';

@Component({
  selector: 'app-all-people',
  templateUrl: './all-people.component.html',
  styleUrls: ['./all-people.component.scss']
})
export class AllPeopleComponent implements OnInit {
  people = [];
  page = 1;
  maxPage: number;

  constructor(private httpRequest: HttpRequestService) { }

  ngOnInit() {
    this.getMostPopularPeople(this.page);
  }

  getMostPopularPeople(page: number) {
    this.httpRequest.getMostPopularPeople(page).subscribe((res: any) => {
      console.log(res);
      this.people = res.results;
      this.maxPage = res.total_pages;
    }, err => {
      console.log(err);
    });
  }

  movePage(move: number) {

    if (move === 0) {
      this.page += 1;
    } else
    if (move === -1) {
      this.page -= 1;
    } else {
      this.page = move;
    }

    console.log('page : ' + this.page);

    this.getMostPopularPeople(this.page);
}

}
