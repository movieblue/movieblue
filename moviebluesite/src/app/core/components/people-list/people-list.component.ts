import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpRequestService } from 'src/app/services/http-request.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Params } from '@angular/router';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss'],
  animations: [
    trigger('peopleAppeared', [
      state('', style({opacity: 1})),
      transition('void => ready', [
        style({opacity: 0, transform: 'translate(-30px, -10px)'}),
        animate('500ms 0s ease-in-out')
      ])
    ])
  ]
})
export class PeopleListComponent implements OnInit, OnDestroy {
  recherche: string;
  people = [];
  rout: Subscription;
  peopleState: string = 'ready';
  page = 1;
  maxPage: number;
  resultats: number;

  constructor(private httpRequest: HttpRequestService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.rout = this.route.params.subscribe(
      (params: Params) => {
         this.recherche = params.recherche;
      }
   );

    this.search();
  }

  search() {
    console.log(this.recherche);
    console.log(encodeURIComponent(this.recherche));    // Encode la recherche pour autoriser les caractères spéciaux

    this.httpRequest.searchPeople(encodeURIComponent(this.recherche), this.page).subscribe((res: any) => {
      console.log(res);
      this.people = res.results;
      this.maxPage = res.total_pages;
      this.resultats = res.total_results;
    }, err => {
        console.log(err);
    });
}

movePage(move: number) {

  if (move === 0) {
    this.page += 1;
  } else
  if (move === -1) {
    this.page -= 1;
  } else {
    this.page = move;
  }

  console.log('page : ' + this.page);

  this.search();
}

ngOnDestroy() {
  this.rout.unsubscribe();
}

}
