import { Component, OnInit, OnDestroy, OnChanges, SimpleChanges, DoCheck, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { Params, ActivatedRoute, UrlSegment } from '@angular/router';
import { HttpRequestService } from 'src/app/services/http-request.service';

@Component({
  selector: 'app-people-details',
  templateUrl: './people-details.component.html',
  styleUrls: ['./people-details.component.scss']
})

export class PeopleDetailsComponent implements OnInit, OnDestroy {
  id: string;
  details: any;
  rout: Subscription;
  credits: any;

  constructor(private httpRequest: HttpRequestService, private route: ActivatedRoute) {}

  ngOnInit() {
    this.rout = this.route.params.subscribe(
      (params: Params) => {
         this.id = params.id;
      }
   );

    console.log('id avant call api :' + this.id);

    this.getPeopleDetails();
    this.getPersonCredits();
  }

  getPeopleDetails() {
    this.httpRequest.getPeopleDetails(this.id).subscribe((res: any) => {
      console.log('people details :' + res);
      this.details = res;
    }, err => {
      console.log(err);
    });
  }

  getPersonCredits() {
    this.httpRequest.getPersonCredits(this.id).subscribe((res: any) => {
      console.log(res);
      this.credits = res;
    }, err => {
      console.log(err);
    });
  }

  ngOnDestroy() {
    this.rout.unsubscribe();
  }

}

