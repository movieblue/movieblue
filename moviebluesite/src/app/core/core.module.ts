import { NgModule} from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertModule } from 'ngx-bootstrap/alert';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../shared/material/material.module';
import { BootstrapmdModule } from '../shared/bootstrapmd/bootstrapmd.module';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { MoviesListComponent } from './components/movies-list/movies-list.component';
import { TvListComponent } from './components/tv-list/tv-list.component';
import { TvDetailsComponent } from './components/tv-details/tv-details.component';
import { PeopleListComponent } from './components/people-list/people-list.component';
import { PeopleDetailsComponent } from './components/people-details/people-details.component';
import { OngletsComponent } from './components/onglets/onglets.component';
import { HomeComponent } from './components/home/home.component';
import { AllTvComponent } from './components/all-tv/all-tv.component';
import { AllPeopleComponent } from './components/all-people/all-people.component';
import { AllMoviesComponent } from './components/all-movies/all-movies.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { PagenotfoundComponent } from './components/pagenotfound/pagenotfound.component';
import {CarouselComponent} from './components/carousel/carousel.component';
import { CoreRoutesModule } from './core.routes.module';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CarouselModule} from 'ngx-bootstrap';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { FooterComponent } from './components/footer/footer.component';
import {NgxPaginationModule} from 'ngx-pagination';

@NgModule({
  declarations: [
    MovieDetailsComponent,
    MoviesListComponent,
    TvListComponent,
    TvDetailsComponent,
    PeopleListComponent,
    PeopleDetailsComponent,
    OngletsComponent,
    CarouselComponent,
    HomeComponent,
    AllTvComponent,
    AllPeopleComponent,
    AllMoviesComponent,
    SidebarComponent,
    PagenotfoundComponent,
    FooterComponent

  ],
  imports: [
    MaterialModule,
    BootstrapmdModule,
    BrowserAnimationsModule,
    NgbModule,
    AlertModule.forRoot(),
    CoreRoutesModule,
    RouterModule,
    NgxPaginationModule,
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    AngularSvgIconModule,
    CarouselModule.forRoot()
  ],
  exports: [
    MovieDetailsComponent,
    MoviesListComponent,
    TvListComponent,
    TvDetailsComponent,
    PeopleListComponent,
    PeopleDetailsComponent,
    OngletsComponent,
    HomeComponent,
    AllTvComponent,
    AllPeopleComponent,
    AllMoviesComponent,
    SidebarComponent,
    CarouselComponent,
    FooterComponent,
  ]
})
export class CoreModule { }
