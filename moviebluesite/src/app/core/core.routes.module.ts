import { Routes, RouterModule } from '@angular/router';
import { AllMoviesComponent } from './components/all-movies/all-movies.component';
import { MovieDetailsComponent } from './components/movie-details/movie-details.component';
import { AllTvComponent } from './components/all-tv/all-tv.component';
import { TvDetailsComponent } from './components/tv-details/tv-details.component';
import { AllPeopleComponent } from './components/all-people/all-people.component';
import { PeopleDetailsComponent } from './components/people-details/people-details.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const coreRoutes: Routes = [
    {
      path: 'movies',
      component: AllMoviesComponent
    },
    {
      path: 'movie-details/:id',
      component: MovieDetailsComponent
    },
    {
        path: 'tv',
        component: AllTvComponent
    },
    {
        path: 'tv-details/:id',
        component: TvDetailsComponent
    },
    {
        path: 'people',
        component: AllPeopleComponent
    },
    {
        path: 'people-details/:id',
        component: PeopleDetailsComponent
    }];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(coreRoutes)],
  exports: [RouterModule]
})
export class CoreRoutesModule { }
