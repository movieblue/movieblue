import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HttpRequestService {
  link: string;

  constructor(private httpClient: HttpClient) {}

  search(query: string, page?: number): Observable<any> {
    this.link = 'https://api.themoviedb.org/3/search/multi?api_key=b69ea114709b77500918c1febf5094df&query=' + query + '&language=fr-FR';

    if (page != null) {
      this.link += '&page=' + page;
    }

    return this.httpClient.get(this.link);
  }

  searchMovies(query: string, page?: number): Observable<any> {
    this.link = 'https://api.themoviedb.org/3/search/movie?api_key=b69ea114709b77500918c1febf5094df&query=' + query + '&language=fr-FR';

    if (page != null) {
      this.link += '&page=' + page;
    }

    return this.httpClient.get(this.link);
  }

  searchTV(query: string, page?: number): Observable<any> {
    this.link = 'https://api.themoviedb.org/3/search/tv?api_key=b69ea114709b77500918c1febf5094df&query=' + query;

    if (page != null) {
      this.link += '&page=' + page;
    }

    return this.httpClient.get(this.link);
  }

  searchPeople(query: string, page?: number): Observable<any> {
    this.link = 'https://api.themoviedb.org/3/search/person?api_key=b69ea114709b77500918c1febf5094df&query=' + query;

    if (page != null) {
      this.link += '&page=' + page;
    }

    return this.httpClient.get(this.link);
  }

  // type = movie ou tv
  getMoviesWithParams(type: string, sort?: string): Observable<any> {
    this.link = 'https://api.themoviedb.org/3/discover/' + type + '?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR';

    if (sort != null) {
      this.link += '&sort_by=' + sort;
    }

    return this.httpClient.get(this.link);
  }

  getLatestMovies(): Observable<any> {
    return this.httpClient
    .get('https://api.themoviedb.org/3/discover/movie?api_key=b69ea114709b77500918c1febf5094df&release_date.gte=2019-09-11'
    + '&release_date.lte=2019-09-18&sort_by=popularity.desc');
  }

  getLatestShows(): Observable<any> {
    return this.httpClient
    .get('https://api.themoviedb.org/3/discover/tv?api_key=b69ea114709b77500918c1febf5094df&air_date.gte=2019-09-11'
    + '&air_date.lte=2019-09-18&sort_by=popularity.desc');
  }

  getMoviesInTheater(): Observable<any> {
    return this.httpClient
    .get('https://api.themoviedb.org/3/movie/now_playing?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getMostPopularMovies(page: number, tri?: string, genres?: Array<string>, dates?: Array<string>) {
    this.link = 'https://api.themoviedb.org/3/discover/movie?api_key=b69ea114709b77500918c1febf5094df'
    + '&language=fr-FR&page=' + page;

    if (tri === '1') {
      this.link += '&sort_by=popularity.desc';
    } else if (tri === '2') {
      this.link += '&sort_by=popularity.asc';
    } else if (tri === '3') {
      this.link += '&sort_by=original_title.desc';
    } else if (tri === '4') {
      this.link += '&sort_by=original_title.asc';
    } else if (tri === '5') {
      this.link += '&sort_by=release_date.desc';
    } else if (tri === '6') {
      this.link += '&sort_by=release_date.asc';
    }

    if (genres.length > 0) {
      this.link += '&with_genres=';

      let i;
      for (i = 0; i < genres.length; i++) {    // génère le morceau de lien contenant les genres pour l'API
        this.link += genres[i] + ',';
      }

      this.link = this.link.substring(0, this.link.length - 1);   // retire la dernière virgule

      console.log(genres);
    }

    if (dates.length > 0) {
        this.link += '&release_date.gte=' + dates[0] + '-01-01' + '&release_date.lte=' + dates[1] + '-12-31';
    }


    console.log('link après modif :' + this.link);
    return this.httpClient.get(this.link);
  }

  getMostPopularTV(page: number, tri?: string, genres?: Array<string>, dates?: Array<string>) {
    this.link = 'https://api.themoviedb.org/3/discover/tv?api_key=b69ea114709b77500918c1febf5094df'
    + '&language=fr-FR&page=' + page;

    if (tri === '1') {
      this.link += '&sort_by=popularity.desc';
    } else if (tri === '2') {
      this.link += '&sort_by=popularity.asc';
    } else if (tri === '3') {
      this.link += '&sort_by=original_title.desc';
    } else if (tri === '4') {
      this.link += '&sort_by=original_title.asc';
    } else if (tri === '5') {
      this.link += '&sort_by=release_date.desc';
    } else if (tri === '6') {
      this.link += '&sort_by=release_date.asc';
    }

    if (genres.length > 0) {
      this.link += '&with_genres=';

      let i;
      for (i = 0; i < genres.length; i++) {    // génère le morceau de lien contenant les genres pour l'API
        this.link += genres[i] + ',';
      }

      this.link = this.link.substring(0, this.link.length - 1);   // retire la dernière virgule

      console.log(genres);
    }

    if (dates.length > 0) {
        this.link += '&air_date.gte=' + dates[0] + '-01-01' + '&air_date.lte=' + dates[1] + '-12-31';
    }


    console.log('link après modif :' + this.link);
    return this.httpClient.get(this.link);
  }

  getMostPopularPeople(page: number) {
    return this.httpClient
    .get('https://api.themoviedb.org/3/person/popular?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR&page=' + page);
  }

  getMovieDetails(id: string): Observable<any> {
    return this.httpClient.get('https://api.themoviedb.org/3/movie/' + id + '?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getRecommendations(id: string, type: string) {
    return this.httpClient
    .get('https://api.themoviedb.org/3/' + type + '/' + id + '/recommendations?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getMovieVideos(id: string) {
    return this.httpClient
    .get('https://api.themoviedb.org/3/movie/' + id + '/videos?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getTVVideos(id: string) {
    return this.httpClient
    .get('https://api.themoviedb.org/3/tv/' + id + '/videos?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getTVDetails(id: string): Observable<any> {
    return this.httpClient.get('https://api.themoviedb.org/3/tv/' + id + '?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getPeopleDetails(id: string): Observable<any> {
    return this.httpClient.get('https://api.themoviedb.org/3/person/' + id + '?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getGenreList(type: string): Observable<any> {
    return this.httpClient.get('https://api.themoviedb.org/3/genre/' + type
     + '/list?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

  getCast(id: string, type: string): Observable<any> {
    return this.httpClient.get('https://api.themoviedb.org/3/' + type + '/' + id
     + '/credits?api_key=b69ea114709b77500918c1febf5094df');
  }

  getPersonCredits(id: string) {
    return this.httpClient.get('https://api.themoviedb.org/3/person/' + id
     + '/combined_credits?api_key=b69ea114709b77500918c1febf5094df&language=fr-FR');
  }

}
